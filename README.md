![Matilda](https://gitee.com/section2014/PicGoPictureBed/raw/master/assets/favico2n.png)

> 你是我一生只会遇见一次的爱情

## 关于名字

[参考Matilda](https://gitee.com/section2014/matilda)

## 目的

#### 着力满足微服务的三大指标，解决四大问题

1. 微服务的三大指标：高可用、高性能、高并发
2. 微服务的四大问题：
   1. 这么多服务，客户端如何访问
   2. 这么多服务，服务与服务之间如何通信
   3. 这么多服务，服务挂了怎么办
   4. 这么多服务，如何管理和监控



#### 最大程度的复用matilda中的代码



## 最后

如果喜欢，欢迎点赞关注。我深知她并不完美。欢迎你与我一起并肩同行。谢谢大家。希望未来，Matilda 会越来越好，不负初心！！！

**来吧，欢迎志同道合的伙伴**。其他的，还希望各位大佬多多指教(*￣︶￣)。

联系我：

- QQ群：783897511
- QQ：1060702785
- 博客：https://section2014.github.io/

